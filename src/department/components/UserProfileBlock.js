import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardMedia from "@material-ui/core/CardMedia";
import userAvatar from "../../images/userAvatar.jpg"



const useStyles = makeStyles({
    depositContext: {
        flex: 1,
    },
    image: {
        minHeight: "100%",
        minWidth: "100%",
        maxHeight: "100%",
        maxWidth: "100%",
    },
});

export default function UserProfileBlock() {
    const classes = useStyles();
    return (
        <React.Fragment>
            <CardMedia
                className={classes.image}
                image={userAvatar}
            />
        </React.Fragment>
    );
}