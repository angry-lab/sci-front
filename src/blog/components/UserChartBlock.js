import React, { PureComponent } from 'react';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
    {
        name: 1.1, uv: 4000, "Кол-во баллов": 2400, amt: 2400,
    },
    {
        name: 2.2, uv: 3000, "Кол-во баллов": 1398, amt: 2210,
    },
    {
        name: 4, uv: 2000, "Кол-во баллов": 9800, amt: 2290,
    },
    {
        name: 8, uv: 2780, "Кол-во баллов": 3908, amt: 2000,
    },
    {
        name: 9, uv: 1890, "Кол-во баллов": 4800, amt: 2181,
    },
    {
        name: 12, uv: 2390, "Кол-во баллов": 3800, amt: 2500,
    },
    {
        name: 13.1, uv: 3490, "Кол-во баллов": 4300, amt: 2100,
    },
    {
        name: 21.1, uv: 2780, "Кол-во баллов": 3908, amt: 2000,
    },
    {
        name: 21.2, uv: 1890, "Кол-во баллов": 4800, amt: 2181,
    },
    {
        name: 22, uv: 2390, "Кол-во баллов": 3800, amt: 2500,
    },
    {
        name: 33.1, uv: 3490, "Кол-во баллов": 4300, amt: 2100,
    },
    {
        name: 33.2, uv: 2780, "Кол-во баллов": 3908, amt: 2000,
    },
    {
        name: 33.3, uv: 1890, "Кол-во баллов": 4800, amt: 2181,
    },
    {
        name: 34.1, uv: 2390, "Кол-во баллов": 3800, amt: 2500,
    },
    {
        name: 34.2, uv: 3490, "Кол-во баллов": 4300, amt: 2100,
    },
    {
        name: 34.3, uv: 1890, "Кол-во баллов": 4800, amt: 2181,
    },
    {
        name: 36, uv: 2390, "Кол-во баллов": 3800, amt: 2500,
    },
    {
        name: 37, uv: 3490, "Кол-во баллов": 4300, amt: 2100,
    },
    {
        name: 71.5, uv: 1890, "Кол-во баллов": 4800, amt: 2181,
    },
    {
        name: 71.3, uv: 2390, "Кол-во баллов": 3800, amt: 2500,
    },
];

export default class UserChartBlock extends PureComponent {
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/q4eonc12/';

    render() {
        return (
            <BarChart
                style={{marginTop: 40, marginBottom: 20}}
                width={1200}
                height={300}
                data={data}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
                barSize={20}
            >
                <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10 }} />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Кол-во баллов" fill="#CCDDFB" background={{ fill: '#eee' }} />
            </BarChart>
        );
    }
}