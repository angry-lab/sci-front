FROM node:12.18.3-alpine3.10 as builder

COPY . /src
WORKDIR /src

ARG public_url

ENV PUBLIC_URL $public_url
RUN npm i && npm run build


FROM nginx:latest

COPY --from=builder /src/build /usr/share/nginx/html
COPY config.example.nginx /etc/nginx/conf.d/default.conf

EXPOSE 80
